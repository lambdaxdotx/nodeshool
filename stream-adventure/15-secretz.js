/*
 * SECRETZ
 */
const crypto = require('crypto')
const tar = require('tar')
const zlib = require('zlib')
const concat = require('concat-stream')

var parser = new tar.Parse()
parser.on('entry', (e) => {
  if (e.type !== 'File') {
    return e.resume()
  }
  let hash = crypto.createHash('md5', { encoding: 'hex' })
  e
    .pipe(hash)
    .pipe(concat((body) => {
      console.log(body + ' ' + e.path)
    }))
  }
)

process.stdin
  .pipe(crypto.createDecipher(
    process.argv[2],
    process.argv[3]
  ))
  .pipe(zlib.createGunzip())
  .pipe(parser)
