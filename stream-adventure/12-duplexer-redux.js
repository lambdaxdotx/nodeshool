/*
 * DUPLEXER REDUX
 */
const duplexer2 = require('duplexer2')
const { Writable } = require('stream');

module.exports = (counter) => {
  var result = {}
  const writable = new Writable({objectMode: true})
  writable._write = (obj, _, next) => {
    result[obj.country] = (result[obj.country] || 0) + 1;
    next()
  }
  writable.end = (done) => {
    counter.setCounts(result)
    // done() // !?
  }
  return duplexer2(
    {objectMode: true},
    writable,
    counter
  )
}
