/*
 * CONCAT
 */
var result = ''
var concat = require('concat-stream')
process.stdin
  .pipe(concat((body) => {
    result = body
      .toString()
      .split('')
      .reverse()
      .join('')
    console.log(result)
  }))
