/*
 * COMBINER
 */
const combine = require('stream-combiner')
const split = require('split')
const through2 = require('through2')
const concat = require('concat-stream')
const zlib = require('zlib')
const gzip = zlib.createGzip()
var group = {
  name: '',
  books: [],
}
module.exports = () => combine(
  split(),
  through2(function transform(line, _, next) {
    try {
      let entry = JSON.parse(line)
      if (entry.type === 'genre') {
        if (group.name !== '') {
          this.push(JSON.stringify(group)+'\n')
        }
        group.name = entry.name
        group.books = []
      } else if (entry.type === 'book') {
        group.books.push(entry.name)
      } else {
      }
    } catch (e) {
      if (e instanceof SyntaxError);
    }
    next()
  }, function flush (cb) {
    this.push(JSON.stringify(group))
    cb()
  }),
  gzip
)
  // concat((body) => {
  //   console.log(body.toString())
  // })
// )

