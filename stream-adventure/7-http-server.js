/*
 * HTTP SERVER
 */
var http = require('http')
var fs = require('fs')
var through2 = require('through2')
var server = http.createServer((req, res) => {
  // fs.createReadStream(process.argv[3])
    // .pipe(res)
  if (req.method === 'POST') {
    req
      .pipe(through2(function (buf, _, next) {
        this.push(buf
          .toString()
          .toUpperCase()
        )
        next()
      }))
      .pipe(res)
    // res.end('')
  }
})
server.listen(process.argv[2])
