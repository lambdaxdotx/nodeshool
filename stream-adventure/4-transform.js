/*
 * TRANSFORM
 */
var fs = require('fs')
var through2 = require('through2')
var stream = through2(
  function write (buffer, encoding, next) {
    this.push(
      Buffer(
        buffer
        .toString()
        .toUpperCase()
      )
    )
    next();
  },
  function end (done) {
    done();
  }
)
// fs.createReadStream(process.argv[2])
process.stdin
  .pipe(stream)
  .pipe(process.stdout)
  // .pipe(process.stdout)
// process.stdin
