/*
 * LINES
 */
var split = require('split')
var through2 = require('through2')
var count = 1;
process.stdin
  .pipe(split())
  .pipe(through2(function (line, _, next) {
    this.push((count % 2 === 1)
      ? line
        .toString()
        .toLowerCase()
        +'\n'
      : line
        .toString()
        .toUpperCase()
        +'\n'
    )
    count = count + 1
    next()
  }))
  .pipe(process.stdout)
