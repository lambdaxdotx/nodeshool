/*
 * DUPLEXER
 */
const { spawn } = require('child_process')
const duplexer2 = require('duplexer2')
module.exports = (cmd, args) => {
  let proc = spawn(cmd, args)
  let dup = duplexer2(proc.stdin, proc.stdout)
  return dup
}
