function* peano () {
  var i = 0
  while (true) {
    yield i
    i = i + 1
  }
}

var f = peano()
console.log( f.next().value )
console.log( f.next().value )
console.log( f.next().value )
