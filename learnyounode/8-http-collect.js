var http = require('http');

http.get(
    process.argv[2],
    (res) => {
        let rawData = '';
        res.setEncoding('utf8');
        res.on('data', (chunk) => { rawData += chunk; })
        res.on('error', console.error);
        res.on('end', () => {
            console.log(rawData.length);
            console.log(rawData);
        });
    }
).on('error', console.error)
