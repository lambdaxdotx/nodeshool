module.exports = function (
    dirName,
    fileExt,
    callback
) {
    var fs = require('fs');
    fs.readdir(
        dirName,
        (err, list) => {
            if (err) {
                return callback(err, list);
            }
            data = list
                .filter((fs) => fs.endsWith('.'+fileExt))
            ;
            callback(null, data);
        }
    );
}

