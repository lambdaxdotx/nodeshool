
const fs = require('fs');
const http = require('http');



/*
var readable = fs.createReadStream(process.argv[2]);
readable
    .pipe(
        map((chunk) => {
            return chunk
                .toString()
                .split('')
                .reverse()
                .join('')
        }))
.on('data', (chunk) => { console.log(chunk.toString()); } );
*/

var map = require('through2-map');

var server = http.createServer((req, res) => {
    var echo;
    if (req.method === 'GET') {
        res.writeHead(200, { 'Content-Type': 'application/json' })
        var url = require('url').parse(req.url, true);
        var date = new Date(url.query.iso);
        if (url.pathname === '/api/parsetime') {
            let obj = {
                hour: date.getHours(),
                minute: date.getMinutes(),
                second: date.getSeconds()
            };
            echo = JSON.stringify(obj);
        } else if (url.pathname === '/api/unixtime') {
            let obj = { unixtime: date.getTime() };
            echo = JSON.stringify(obj);
        } else {
            echo = '';
        }
    } else {
        echo = '';
    }
    res.end(echo); 
});

server.listen(Number(process.argv[2]));
