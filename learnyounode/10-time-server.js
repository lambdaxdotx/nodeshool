const net = require('net');

var pad2zeros = ((n) => String(n).padStart(2, "0"));

var server = net.createServer((socket) => {
    var date = new Date();
    var output = String(date.getFullYear())
        + '-' + pad2zeros(date.getMonth()+1)
        + '-' + pad2zeros(date.getDate())
        + ' ' + pad2zeros(date.getHours())
        + ':' + pad2zeros(date.getMinutes())
        ;
    socket.end(output+'\n');
}).on('error', (err) => { 
    throw err; 
});

server.listen(Number(process.argv[2]));
