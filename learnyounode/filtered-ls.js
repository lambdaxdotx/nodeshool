var fs = require('fs');

var dirName = process.argv[2];
var fileExt = process.argv[3];

fs.readdir(
    dirName,
    (err, list) => {
        if (err) throw err;
        list
            .filter((fs) => fs.endsWith('.'+fileExt))
            .forEach((x) => console.log(x))
    }
)
