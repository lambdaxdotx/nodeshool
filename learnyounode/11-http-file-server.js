
const fs = require('fs');
const http = require('http');


/* 
function dumpTextAsync(file, callback) {
    const readable = fs.createReadStream(file);
    let rawData = '';
    readable.on('data', (chunk) => {
        rawData = rawData + chunk.toString();
    });
    readable.on('end', () => {
        callback(rawData);
    });

}
dumpTextAsync(
    process.argv[2],
    console.log
);
*/


var server = http.createServer((req, res) => {
    // res.writeHead(200, {'Content-Type': 'text/plain'});   
    // res.end('okay'); 
    let readable = fs.createReadStream(process.argv[3]);
    readable.pipe(res);
});

server.listen(Number(process.argv[2]));
