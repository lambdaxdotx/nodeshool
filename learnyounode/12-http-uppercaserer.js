
const fs = require('fs');
const http = require('http');



/*
var readable = fs.createReadStream(process.argv[2]);
readable
    .pipe(
        map((chunk) => {
            return chunk
                .toString()
                .split('')
                .reverse()
                .join('')
        }))
.on('data', (chunk) => { console.log(chunk.toString()); } );
*/

var map = require('through2-map');

var server = http.createServer((req, res) => {
    if (req.method === 'POST') {
        req
            .pipe(map((chunk) => {
                console.log(chunk.toString());
                return chunk.toString().toUpperCase()
            }))
            .pipe(res)
            ;
    } else {
        res.end(); 
    }
});

server.listen(Number(process.argv[2]));
