var http = require('http')

var output = []

var finished = (data, key) => {
  output[key] = data
  if (output
    .filter((val) => (val !== undefined))
    .length ===
    (process.argv.length - 2)
  ) {
    var x = output
      .map((x) => { console.log(x) })
  }
}

var reg = (url, key) => {
  http.get(
    url,
    (res) => {
      let rawData = ''
      res.on('data', (chunk) => {
        rawData += chunk
      })
      res.on('end', () => {
        finished(rawData, key)
      })
    }
  ).on('error', console.error)
}

process
  .argv
  .slice(2)
  .forEach(reg)

/*
 * There's a fundamental paradigm shift when you think in the node.js way.
 * As had been writtedn and said about in node
 *      " Everything runs in parallel except your code" .
 * JS is single threaded and hence if you make that thread sleep , everything blocks.
 *
 * But if you model your problem in a natural way ,
 * it would be to design an asysnc operation that would take its time to run
 * and when its finished let it inform you of the same. Rather than you waiting for it to finish.
 */
