/* duckCount(...args) */
module.exports = (...args) =>
  args
    .filter((x) => (
      Object.prototype.hasOwnProperty.call(x, 'quack')
    ))
    .length
/* ref:
 * https://github.com/hapijs/hapi/issues/3280
 */
