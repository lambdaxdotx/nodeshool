/*
 * reduce
 */
module.exports = (arr, fn, initial) => {
  function recur (a) {
    // console.log(a)
    let [car, ...cdr] = a
    return (a.length === 0)
      ? initial
      : fn(recur(cdr), car)
  }
  return recur(arr)
}
