/*
 * Partial Application without Bind
 */
module.exports = (namespace) =>
  (...msg) => console.log(namespace, ...msg)

