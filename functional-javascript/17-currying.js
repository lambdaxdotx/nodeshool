/*
 * Currying
 */
module.exports = (fn, n) => {
  if (n === undefined) {
    n = fn.length
  }
  var recur = (f, k) => ((k === 0)
    ? f()
    : (x) => recur(
      (...y) => f(x, ...y), // f.bind(this, x),
      k - 1
    )
  )
  return recur(fn, n)
}
