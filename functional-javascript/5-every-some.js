// module.exports = goodUsers =>
//    submittedUsers => {
//        sub = submittedUsers.map( o => o.id );
//        good = goodUsers.map( o => o.id );
//        return sub
//            .filter( n => !good.includes(n) )
//            .length === 0
//        ;
//    }
// ;

module.exports = goodUsers =>
  submittedUsers => {
    let sub = submittedUsers.map(o => o.id)
    let good = goodUsers.map(o => o.id)
    return sub
      .every(s => good.some(g => g === s))
  }
