/*
 * Function Spies
 */
module.exports = (target, method) => {
  var stored = target[method]
  var obj = { count: 0 }
  //target[method] = (...argv) => {
  //  ++obj.count
  //  return stored(...argv)
  //}
  target[method] = function () {
    ++obj.count
    return stored.apply(this, arguments)
  }
  return obj
}
