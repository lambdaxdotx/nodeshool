/*
 * Implement Map with Reduce
 */
module.exports = (arr, fn) =>
  arr
    .reduce(
      (acc, curr) => [...acc, fn(curr)],
      []
    )
