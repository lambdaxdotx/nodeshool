/*
 * Async Loops
 */
module.exports = (userIds, load, done) => {
  let ids = [...Array(userIds.length).keys()]
  let users = [];
  ids
    .forEach((i) => {
      // let k = i
      load(userIds[i], (result) => {
        users[i] = result
      })
    })
  done(users)
}
