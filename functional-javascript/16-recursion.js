/*
 * Recursion
 */
/*
var loremIpsum = {
  'name': 'lorem-ipsum',
  'version': '0.1.1',
  'dependencies': {
    'optimist': {
      'version': '0.3.7',
      'dependencies': {
        'wordwrap': {
          'version': '0.0.2',
        },
      },
    },
    'inflection': {
      'version': '1.2.6',
    },
  },
}
console.log(
  Object.keys(loremIpsum)
)
*/
module.exports = (tree) => {
  var result = []
  function recur (obj) {
    // console.log(obj.name + '@' + obj.version)
    result.push(obj.name + '@' + obj.version)
    var isLeaf = (obj) => !('dependencies' in obj)
    if (isLeaf(obj)) {
    } else {
      Object.keys(obj.dependencies).forEach((key) => {
        obj.dependencies[key].name = key
        recur(obj.dependencies[key])
      })
    }
  }
  if (!('name' in tree)) {
    tree.name = 'dummy'
  }
  recur(tree)
  return Array
    .from(new Set(result
      .slice(1)
      .sort())
    )
}
