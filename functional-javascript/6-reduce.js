/*
 * countWords
 */
module.exports = (arr) =>
  arr
    .reduce(
      (acc, curr) => {
        let tmp = acc;
        tmp[curr] = ++tmp[curr] || 1
        return tmp
      },
      {}
    )

