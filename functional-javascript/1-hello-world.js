
module.exports = input =>
  input
    .split('')
    .map(c => c.toUpperCase())
    .join('')
