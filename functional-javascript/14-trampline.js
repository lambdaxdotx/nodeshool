/*
 * Trampoline
 */
module.exports = (operation, num) => {
  const trampoline = (fn) => {
    do {
      fn = fn()
    } while (typeof fn === 'function')
    return fn
  }
  /*
  const repeat = (op, n) => (
    (n <= 0)
      ? null
      : (op(), () => repeat(op, n - 1))
  )
  */
  const repeat = (op, n) =>
    () => (
      (n <= 0)
        ? null
        : (op(), repeat(op, n - 1))
    )
  return trampoline(() => repeat(operation, num))
}
