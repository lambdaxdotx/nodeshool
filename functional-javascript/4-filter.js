module.exports = ar =>
    ar
        .map( x => x.message )
        .filter( msg => msg.length <50 )
;
