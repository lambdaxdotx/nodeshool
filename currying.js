
function money (a, b, c) {
  return 1 * a
    + 10 * b
    + 100 * c
}

/* hard code currying */
var money1 = (...y) => money(5, ...y)
var money2 = (...y) => money1(7, ...y)
var money3 = (...y) => money2(9, ...y)
console.log(
  money3()
)

/* Babel way */
function curry(f, x) {
  return (function () {
    for (
      var _len = arguments.length, y = Array(_len), _key = 0;
      _key < _len;
      _key++) {
      y[_key] = arguments[_key];
    }
    return f.apply(undefined, [x].concat(y))
  })
}
var mom1 = curry(money, 5)
var mom2 = curry(mom1, 7)
var mom3 = curry(mom2, 9)
console.log(
  mom3()
)

/* more compact way */
function curryBind(f, x) {
  return f.bind(undefined, x)
}
var cash1 = curryBind(money, 5)
var cash2 = curryBind(cash1, 7)
var cash3 = curryBind(cash2, 9)
console.log(
  cash3()
)

/* ES6 */
function curryEs6(f, x) {
  return (...y) => f(x, ...y)
}
var sicp1 = curryEs6(money, 5)
var sicp2 = curryEs6(sicp1, 7)
var sicp3 = curryEs6(sicp2, 9)
console.log(
  sicp3()
)
