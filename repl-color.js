// you'll need the util module
var util = require('util');

//// let's look at the defaults:
//util.inspect.styles
//
//{ special: 'cyan',
//  number: 'yellow',
//  boolean: 'yellow',
//  undefined: 'grey',
//  null: 'bold',
//  string: 'green',
//  date: 'magenta',
//  regexp: 'red' }
//
//// what are the predefined colors?
//util.inspect.colors
//
//{ bold: [ 1, 22 ],
//  italic: [ 3, 23 ],
//  underline: [ 4, 24 ],
//  inverse: [ 7, 27 ],
//  white: [ 37, 39 ],
//  grey: [ 90, 39 ],
//  black: [ 30, 39 ],
//  blue: [ 34, 39 ],
//  cyan: [ 36, 39 ],
//  green: [ 32, 39 ],
//  magenta: [ 35, 39 ],
//  red: [ 31, 39 ],
//  yellow: [ 33, 39 ] }

util.inspect.colors.lightmagenta = [95,39];
util.inspect.styles.date = 'lightmagenta';

// start the repl
require('repl').start({});
