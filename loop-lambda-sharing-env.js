/*
 * 1) Pitfall
 */
function f () {
  var result = []
  for (var i = 0; i < 10; i++) {
    result.push(() => i)
  }
  return result
}
console.log(
  f()[3]()
)

/*
 * 2) Method I: IIFE closure
 */
function g () {
  var result = []
  for (var i = 0; i < 10; i++) {
    (() => {
      var pos = i
      result.push(() => pos)
    })()
  }
  return result
}
console.log(
  g()[3]()
)

/*
 * 3) Method II: let
 */
function h () {
  var result = []
  for (var i = 0; i < 10; i++) {
    let pos = i
    result.push(() => pos)
  }
  return result
}
console.log(
  h()[3]()
)

/*
 * 4) Method III: forEach
 */
function e () {
  var result = []
  var count = [...Array(10).keys()]
  count
    .forEach((i) => {
      result.push(() => i)
    })
  return result
}
console.log(
  e()[3]()
)
