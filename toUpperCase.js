const through2 = require('through2')
process.stdin
  .pipe(through2(function (buf, _, next) {
    this.push(buf.toString().toUpperCase())
    next()
  }))
  .pipe(process.stdout)
